[![status-badge](https://ci.codeberg.org/api/badges/wlkr/walker/status.svg)](https://ci.codeberg.org/wlkr/walker)

_Walker_ is a mathematical tool to analyze and design the behavior of
stochastic differential equations.

Using the [Charm++](http://charmplusplus.org/) runtime system, it employs
_asynchronous_ (or non-blocking) parallel programming and decomposes
computational problems into a large number of work units (that may be more than
the available number of processors) enabling _arbitrary overlap_ of parallel
computation, communication, input, and output. Then the runtime system
_dynamically_ and _automatically_ homogenizes computational load across the
simulation distributed across many computers.

More details at [wlkr.cc](https://wlkr.cc) or [doc/pages/mainpage.dox](doc/pages/mainpage.dox).
