################################################################################
#
# \file      ConfigureDataLayout.cmake
# \copyright 2012-2015 J. Bakosi,
#            2016-2018 Los Alamos National Security, LLC.,
#            2019-2021 Triad National Security, LLC.
#            2022-2025 J. Bakosi
#            All rights reserved. See the LICENSE file for details.
# \brief     Configure data layouts
#
################################################################################

# Configure data layout for particle data

# Available options
set(PARTICLE_DATA_LAYOUT_VALUES "particle" "equation")
# Initialize all to off
set(PARTICLE_DATA_LAYOUT_AS_PARTICLE_MAJOR off)  # 0
set(PARTICLE_DATA_LAYOUT_AS_EQUATION_MAJOR off)  # 1
# Set default and select from list
set(PARTICLE_DATA_LAYOUT "particle" CACHE STRING "Particle data layout. Default: (particle-major). Available options: ${PARTICLE_DATA_LAYOUT_VALUES}(-major).")
SET_PROPERTY (CACHE PARTICLE_DATA_LAYOUT PROPERTY STRINGS ${PARTICLE_DATA_LAYOUT_VALUES})
STRING (TOLOWER ${PARTICLE_DATA_LAYOUT} PARTICLE_DATA_LAYOUT)
LIST (FIND PARTICLE_DATA_LAYOUT_VALUES ${PARTICLE_DATA_LAYOUT} PARTICLE_DATA_LAYOUT_INDEX)
# Evaluate selected option and put in a define for it
IF (${PARTICLE_DATA_LAYOUT_INDEX} EQUAL 0)
  set(PARTICLE_DATA_LAYOUT_AS_PARTICLE_MAJOR on)
ELSEIF (${PARTICLE_DATA_LAYOUT_INDEX} EQUAL 1)
  set(PARTICLE_DATA_LAYOUT_AS_EQUATION_MAJOR on)
ELSEIF (${PARTICLE_DATA_LAYOUT_INDEX} EQUAL -1)
  MESSAGE(FATAL_ERROR "Particle data layout '${PARTICLE_DATA_LAYOUT}' not supported, valid entries are ${PARTICLE_DATA_LAYOUT_VALUES}(-major).")
ENDIF()
message(STATUS "Particle data layout: " ${PARTICLE_DATA_LAYOUT} "(-major)")
